import React from 'react';
import './style.css';

class Card extends React.Component {

  handleInputChange = (id, e) => {
    this.props.handleInputChange(id, e);
  }

  deleteCard = (id) => {
    this.props.deleteCard(id);
  }

  render() {
    const {
      cardInfo: { id, valid, name, email, radioVal, checked },
    } = this.props;
    return (
      <div className="ui cards" >
        <div className="card">
          <div className="content">
            <form className="ui form">
              <div className="field">
                <label>Name</label>
                <input onChange={this.handleInputChange.bind(this, id)} name="name" type="text" placeholder="Name" value={name} required/>
              </div>
              <div className="field">
                <label>Email:</label>
                <input onChange={this.handleInputChange.bind(this, id)} name="email" type="text"  placeholder="Email" value={email} required/>
                {!valid ? <div className="ui red message">Not valid email</div> : null}
              </div>
              <div className="field">
                <div className="ui checkbox">
                  <input onChange={this.handleInputChange.bind(this, id)} name="checked" type="checkbox"  checked={checked} required/>
                  <label>This is checkbox</label>
                </div>
              </div>
              <div className="inline fields">
              <div className="ui radio checkbox">
                <input onChange={this.handleInputChange.bind(this, id)} name="radioVal" type="radio" value="opt1" checked={radioVal === 'opt1'}  required/>
                <label >Option 1</label> 
              </div>
              <div className="field">
              <div className="ui radio checkbox">
                <input onChange={this.handleInputChange.bind(this, id)} name="radioVal" type="radio" value="opt2" checked={radioVal === 'opt2'} required/>
                <label>Option 2</label>
                </div>
              </div>
            </div>
            </form>
          </div>
          <div className="extra content">
          <div className="ui bottom attached button" onClick={this.deleteCard.bind(this, id)}>
            <i className="minus circle icon"></i>
            Delete Card
          </div>
          </div>
        </div>
      </div>
    );
  }
};

export default Card;

