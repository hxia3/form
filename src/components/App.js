import React from 'react';
import Card from './Card';
import './style.css';
const uuidv4 = require("uuid/v4");
class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      cardList: [
        { 
          id: uuidv4(),
          name: 'test1',
          email: 'test1@test.com',
          radioVal: 'opt1',
          checked: true,
          valid: true,
        },
        { 
          id: uuidv4(),
          name: 'test2',
          email: 'test2@test.com',
          radioVal: 'opt2',
          checked: false,
          valid: true,
        },
      ],
    };
  }
  
  handleInputChange= (id, e) => {
    const { cardList } = this.state;
    const newCardList = cardList.map((card) => {
      if(card.id !== id) {
        return card;
      } else {
        return e.target.name !== 'checked' ? Object.assign(card, {
          [e.target.name]: e.target.value,
        }) :  Object.assign(card, {
          checked: !card.checked,
        });
      }
    });
    this.setState({
      cardList: newCardList,
    });
  }

  renderList = () => {
    const { cardList } = this.state;
    return  cardList.map((cardInfo, idx) => {
      return (
        <Card deleteCard={this.deleteCard} handleInputChange={this.handleInputChange} cardInfo={cardInfo} key={idx} />
      );
    })

  }

  addForm = () => {
    this.setState({
      cardList: [...this.state.cardList, {
        id: uuidv4(),
        name: 'test',
        email: 'test@test.com',
        radioVal: 'opt1',
        checked: true,
        valid: true,
      }]
    })
  }

  deleteCard = (id) => {
    const newCardList = this.state.cardList.filter(card => card.id !== id);
    this.setState({
      cardList: newCardList, 
    });
  }

  validate = () => {
    let emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const { cardList } = this.state;
    const newCardList = cardList.map((card) => {
      if(emailRegex.test(card.email)) {
        return card;
      } else {
        return Object.assign(card, {
          valid: false,
        });
      }
    });
    this.setState({
      cardList: newCardList,
    },() => {
      let firstErrorNode = document.getElementsByClassName('red')[0];
      const y = firstErrorNode.getBoundingClientRect().top + window.scrollY;
      window.scroll({
        top: y - 100,
        behavior: 'smooth'
      });
    });
  }

  render() {
    return (
      <div className="main">
        <div className="main__list">
          {this.renderList()}
        </div>
        <div className="main__buttons" >
          <button onClick={this.addForm} className="ui secondary basic button">Add</button>
          <button onClick={this.validate} className="ui primary basic button">submit</button>
        </div>
      </div>
    )
  }
}

export default App;